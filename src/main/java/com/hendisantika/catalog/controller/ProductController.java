package com.hendisantika.catalog.controller;

import com.hendisantika.catalog.entity.Product;
import com.hendisantika.catalog.repository.ProductRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : catalog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/01/18
 * Time: 21.55
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/api/products/")
public class ProductController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductRepo productRepo;


    @GetMapping
    public Page<Product> findProducts(Pageable page) {
        return productRepo.findAll(page);
    }

    @GetMapping("{id}")
    public Product findById(@PathVariable("id") Product p) {
        //Product p = productDao.findOne(id); // tidak perlu lagi, karena sudah disediakan Spring Data JPA
        logger.info("Mencari product dengan id {}", p.getId());
        return p;
    }


}
