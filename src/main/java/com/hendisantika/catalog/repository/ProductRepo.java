package com.hendisantika.catalog.repository;

import com.hendisantika.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : catalog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/01/18
 * Time: 21.54
 * To change this template use File | Settings | File Templates.
 */
public interface ProductRepo extends PagingAndSortingRepository<Product, String> {
}
